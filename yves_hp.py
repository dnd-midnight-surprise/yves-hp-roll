#!/usr/bin/env python3

# Example
# Roll hit dice for level 6 Barbarian
#   ./yves_hp --levels 6 12

import argparse
import random
import math

def main():
    random.seed()
    parser = argparse.ArgumentParser(description='Yves style DND hp')

    parser.add_argument('--levels', type=int, default=1, help ="number of levels to roll for")
    parser.add_argument('hitdie', type=int, help = "class hit die, enter '6' for 1d6")

    args = parser.parse_args()

    print(f"Rolling Yves style hp for {args.levels}d{args.hitdie}")
    ans = 0
    for i in range(args.levels):
        ans += math.ceil(random.randint(1,args.hitdie)/2) + args.hitdie/2

    ans = int(ans)

    print(ans)



if __name__ == "__main__":
    main()
